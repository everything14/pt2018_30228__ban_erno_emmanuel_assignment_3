package model;

public class Comanda {
		
	private int id;
	private int idClient;
	private int idProdus;
	
	
	public Comanda(int id, int idClient, int idProdus) {
		super();
		this.id = id;
		this.idClient = idClient;
		this.idProdus = idProdus;
	}

	public Comanda(int idClient, int idProdus) {
		this.idClient = idClient;
		this.idProdus = idProdus;
	}

	public Comanda() {

	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the idClient
	 */
	public int getIdClient() {
		return idClient;
	}
	/**
	 * @param idClient the idClient to set
	 */
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	/**
	 * @return the idProdus
	 */
	public int getIdProdus() {
		return idProdus;
	}
	/**
	 * @param idProdus the idProdus to set
	 */
	public void setIdProdus(int idProdus) {
		this.idProdus = idProdus;
	}
	

}
