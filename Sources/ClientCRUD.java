package bussinessLogic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import database.DataBaseTablesAndConnection;
import model.Client;

public class ClientCRUD {

	/** INSERT ELEMENTS IN TABLE **/
	public void inserareClient(Client c) throws Exception {
		final String nume = c.getNume();
		final int varsta = c.getVarsta();
		final String adresa = c.getAddresa();
		try {
			Connection con = DataBaseTablesAndConnection.getConnection();
			PreparedStatement posted = con.prepareStatement("INSERT INTO client(nume,varsta,adresa) VALUES ('" + nume
					+ "', '" + varsta + "', '" + adresa + "')");
			posted.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			System.out.println("Client adaugat cu success!!");
		}
	}

	/** DELETE ELEMENTS FROM TABLE **/
	public void stergereClient(int id) throws Exception {
		try {
			Connection con = DataBaseTablesAndConnection.getConnection();
			PreparedStatement posted = con.prepareStatement("DELETE FROM client WHERE id=" + id);
			posted.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			System.out.println("A fost sters un client");
		}
	}

	// ** UPDATE ELEMENTS FROM TABLE **//
	public void updateClient(Client c) throws Exception {
		try {
			Connection con = DataBaseTablesAndConnection.getConnection();
			PreparedStatement posted = con.prepareStatement("UPDATE client SET nume='" + c.getNume() + "', varsta='"
					+ c.getVarsta() + "', adresa='" + c.getAddresa() + "' WHERE id=" + c.getId() + "");
			posted.executeUpdate();
			System.out.println("Clientul a primit update cu success");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	// VIEW A CLIENT FROM TABLE
	public Client selectareUnClientDupaID(int id) throws Exception {

		Client c = new Client();
		try {
			Connection con = DataBaseTablesAndConnection.getConnection();
			PreparedStatement posted = con.prepareStatement("SELECT * FROM client WHERE id=" + id);
			ResultSet rez = posted.executeQuery();
			while (rez.next()) {
				c.setNume(rez.getString("nume"));
				c.setVarsta(rez.getInt("varsta"));
				c.setAddresa(rez.getString("adresa"));
				c.setId(id);
			}
			return c;

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			System.out.println("A fost ales un client din baza de date");
		}
		return null;
	}

	// VIEW ALL CLIENTS
	public LinkedList<Client> selectareTotiClientii() throws Exception {
		LinkedList<Client> listaClienti = new LinkedList<Client>();
		Connection con = DataBaseTablesAndConnection.getConnection();
		PreparedStatement posted = con.prepareStatement("SELECT * FROM client ");
		ResultSet rez = posted.executeQuery();
		while (rez.next()) {
			Client aux = new Client(rez.getInt(1), rez.getString(2), rez.getInt(3), rez.getString(4));
			listaClienti.add(aux);
		}
		return listaClienti;
	}

}
