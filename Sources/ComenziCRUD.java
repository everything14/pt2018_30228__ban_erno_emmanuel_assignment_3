package bussinessLogic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import database.DataBaseTablesAndConnection;
import model.Client;
import model.Comanda;
import model.Produs;

public class ComenziCRUD {

	/** DELETE ELEMENTS FROM TABLE **/
	public void stergereComanda(int id) throws Exception {
		try {
			Connection con = DataBaseTablesAndConnection.getConnection();
			PreparedStatement posted = con.prepareStatement("DELETE FROM comanda WHERE id=" + id);
			posted.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			System.out.println("A fost sterarsa o comanda");
		}
	}

	public void inserareComanda(Comanda o) throws Exception {
		// final int idComanda = o.getId();
		final int idClient = o.getIdClient();
		final int idProdus = o.getIdProdus();
		try {
			Connection con = DataBaseTablesAndConnection.getConnection();
			PreparedStatement posted = con.prepareStatement(
					"INSERT INTO comanda(idClient,idProdus) VALUES (" + idClient + "," + idProdus + ")");
			posted.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			System.out.println("Comanda adaugata cu success!!");
		}
	}

	public void updateComanda(Comanda c) throws Exception {
		try {
			Connection con = DataBaseTablesAndConnection.getConnection();
			PreparedStatement posted = con.prepareStatement("UPDATE Comanda SET idClient=" + c.getIdClient()
					+ ",idProdus=" + c.getIdProdus() + "WHERE id=" + c.getId());
			posted.executeUpdate();
			System.out.println("Comanda a primit update cu success");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public Comanda selectareComandaDupaID(int id) throws Exception {

		Comanda c = new Comanda();
		try {
			Connection con = DataBaseTablesAndConnection.getConnection();
			PreparedStatement posted = con.prepareStatement("SELECT * FROM comanda WHERE id=" + id);
			ResultSet rez = posted.executeQuery();
			while (rez.next()) {
				c.setIdClient(rez.getInt("idClient"));
				c.setIdProdus(rez.getInt("idProdus"));
				c.setId(id);
			}
			return c;

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			System.out.println("A fost ales un produs din baza de date");
		}
		return null;
	}

	public LinkedList<Comanda> selectareToateComenzile() throws Exception {
		LinkedList<Comanda> listaComenzi = new LinkedList<Comanda>();
		Connection con = DataBaseTablesAndConnection.getConnection();
		PreparedStatement posted = con.prepareStatement("SELECT * FROM comanda ");
		ResultSet rez = posted.executeQuery();
		while (rez.next()) {
			Comanda aux = new Comanda(rez.getInt(1), rez.getInt(2), rez.getInt(3));
			listaComenzi.add(aux);
		}
		return listaComenzi;
	}

}
