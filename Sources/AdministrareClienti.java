package view;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import bussinessLogic.ClientCRUD;
import model.Client;

import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.awt.event.ActionEvent;

public class AdministrareClienti {
	/**REFLECTION**/
	List<Field> privateFields = new ArrayList<>();
	Field[] allFields = Client.class.getDeclaredFields();
	
	private JFrame frame;
	

	LinkedList<Client> listaClienti = new LinkedList<Client>();
	ClientCRUD crud = new ClientCRUD();
	private JTextField textField_nume;
	private JTextField textField_varsta;
	private JTextField textField_adresa;
	private JTextField textField_stergeID;
	int total_fields=0;

	public AdministrareClienti() {
		initialize();
		frame.setVisible(true);
	}

	private void initialize() {
		
		/**REFLECTION**/
		for (Field field : allFields) {
		    if (Modifier.isPrivate(field.getModifiers())) {
		        privateFields.add(field);
		        total_fields++;
		    }
		}
		/**REFLECTION**/
		String[] columnNames=new String[total_fields];
		for(int x=0;x<privateFields.size();x++) {
			columnNames[x]=privateFields.get(x).getName();
		}
		Object[][] data = { { "9999999", "Test", "Test", "999999" } };
		JTable table = new JTable(new DefaultTableModel(data, columnNames));
		DefaultTableModel model_table = (DefaultTableModel) table.getModel();
		
		frame = new JFrame();
		try {
			listaClienti=crud.selectareTotiClientii();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		frame.getContentPane().setBackground(Color.LIGHT_GRAY);
		frame.setBounds(100, 100, 791, 445);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 30, 354, 349);
		scrollPane.setBackground(Color.RED);
		frame.getContentPane().add(scrollPane);
		table.setBackground(Color.PINK);

		scrollPane.setColumnHeaderView(table);
		scrollPane.setViewportView(table);

		JLabel lblTabelClienti = new JLabel("Tabel Clienti");
		lblTabelClienti.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblTabelClienti.setBounds(130, 5, 117, 14);
		frame.getContentPane().add(lblTabelClienti);
		
		JLabel lblNume = new JLabel("Nume");
		lblNume.setBounds(411, 49, 46, 14);
		frame.getContentPane().add(lblNume);
		
		JLabel lblVarsta = new JLabel("Varsta");
		lblVarsta.setBounds(411, 74, 46, 14);
		frame.getContentPane().add(lblVarsta);
		
		JLabel lblAdresa = new JLabel("Adresa");
		lblAdresa.setBounds(411, 99, 46, 14);
		frame.getContentPane().add(lblAdresa);
		
		textField_nume = new JTextField();
		textField_nume.setBounds(467, 46, 86, 20);
		frame.getContentPane().add(textField_nume);
		textField_nume.setColumns(10);
		
		textField_varsta = new JTextField();
		textField_varsta.setBounds(467, 71, 86, 20);
		frame.getContentPane().add(textField_varsta);
		textField_varsta.setColumns(10);
		
		textField_adresa = new JTextField();
		textField_adresa.setBounds(467, 96, 86, 20);
		frame.getContentPane().add(textField_adresa);
		textField_adresa.setColumns(10);
		
		JButton btnAdaugaClient = new JButton("Adauga client");
		btnAdaugaClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Client c= new Client();
				c.setAddresa(textField_adresa.getText());
				c.setNume(textField_nume.getText());
				c.setVarsta(Integer.parseInt(textField_varsta.getText()));
				try {
					crud.inserareClient(c);
					listaClienti.add(c);
					refresh();
					int i=listaClienti.size()-1;
					DefaultTableModel aux = (DefaultTableModel) table.getModel();
					aux.addRow(new Object[] { listaClienti.get(i).getId(), listaClienti.get(i).getNume(),
					listaClienti.get(i).getVarsta(), listaClienti.get(i).getAddresa() });
					table.setModel(aux);					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnAdaugaClient.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnAdaugaClient.setBounds(585, 70, 147, 23);
		frame.getContentPane().add(btnAdaugaClient);
		
		JButton btnNewButton = new JButton("Sterge client");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					crud.stergereClient(Integer.parseInt(textField_stergeID.getText()));
					int i=table.getSelectedRow();
					model_table.removeRow(i);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(585, 173, 117, 23);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblId = new JLabel("Id");
		lblId.setBounds(429, 177, 46, 14);
		frame.getContentPane().add(lblId);
		
		textField_stergeID = new JTextField();
		textField_stergeID.setBounds(467, 174, 86, 20);
		frame.getContentPane().add(textField_stergeID);
		textField_stergeID.setColumns(10);
		for (int i = 0; i < listaClienti.size(); i++) {
			model_table.addRow(new Object[] { listaClienti.get(i).getId(), listaClienti.get(i).getNume(),
					listaClienti.get(i).getVarsta(), listaClienti.get(i).getAddresa() });
		}
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	public void refresh() throws Exception {
		this.listaClienti=crud.selectareTotiClientii();
	}
}
