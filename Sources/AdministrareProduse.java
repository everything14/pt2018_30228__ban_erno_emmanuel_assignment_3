package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.RepaintManager;
import javax.swing.table.DefaultTableModel;

import bussinessLogic.ProdusCRUD;
import model.Client;
import model.Produs;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.awt.event.ActionEvent;

public class AdministrareProduse {
	
	/**REFLECTION**/
	List<Field> privateFields = new ArrayList<>();
	Field[] allFields = Produs.class.getDeclaredFields();
	int total_fields=0;
	
	public int size_lista;

	private JFrame frame;
	private JTextField textField_nume;
	private JTextField textField_cantitate;
	private JTextField textField_pret;

	LinkedList<Produs> listaProduse= new LinkedList<Produs>();
	ProdusCRUD crud=new ProdusCRUD();
	private JTextField textField_stergere;
	private JTextField textField_updateID;

	public AdministrareProduse() {
		initialize();
		frame.setVisible(true);
	}

	private void initialize() {
		
		
		/**REFLECTION**/
		for (Field field : allFields) {
		    if (Modifier.isPrivate(field.getModifiers())) {
		        privateFields.add(field);
		        total_fields++;
		    }
		}
		/**REFLECTION**/
		String[] columnNames=new String[total_fields];
		for(int x=0;x<privateFields.size();x++) {
			columnNames[x]=privateFields.get(x).getName();
		}
		
		Object[][] data = { { "9999999", "Test", "Test", "999999" } };
		JTable table_1 = new JTable(new DefaultTableModel(data, columnNames));
		DefaultTableModel model_table = (DefaultTableModel) table_1.getModel();
		try {
			listaProduse=crud.selectareToateProdusele();
		} catch (Exception e1) {

			e1.printStackTrace();
		}
		
		
		
		

		frame = new JFrame();
		frame.setBounds(100, 100, 688, 466);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton btnInserareProdus = new JButton("Inserare Produs");
		btnInserareProdus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Produs p= new Produs(Integer.parseInt(textField_cantitate.getText()),Double.parseDouble(textField_pret.getText()),textField_nume.getText());
				size_lista=listaProduse.size();
				Produs aux=new Produs();
				try {
					crud.inserareProdus(p);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				listaProduse.add(p);
				try {
				aux=crud.selectareProdusDupaID(size_lista+2);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				DefaultTableModel nou_model = (DefaultTableModel) table_1.getModel();
				//nou_model.addRow(new Object[]{aux.getId(),listaProduse.get(size_lista).getNume(),listaProduse.get(size_lista).getPret(),listaProduse.get(size_lista).getCantitate()});	
				model_table.fireTableDataChanged();
				model_table.fireTableStructureChanged();
				table_1.repaint();
				//table_1.setModel(nou_model);
			}
		});
		btnInserareProdus.setBackground(Color.RED);
		btnInserareProdus.setBounds(497, 131, 126, 23);
		frame.getContentPane().add(btnInserareProdus);

		JLabel lblNume = new JLabel("Nume");
		lblNume.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNume.setBounds(417, 53, 46, 14);
		frame.getContentPane().add(lblNume);

		JLabel lblCantitate = new JLabel("Cantitate");
		lblCantitate.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCantitate.setBounds(417, 78, 61, 14);
		frame.getContentPane().add(lblCantitate);

		JLabel lblPret = new JLabel("Pret");
		lblPret.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPret.setBounds(417, 103, 46, 14);
		frame.getContentPane().add(lblPret);

		textField_nume = new JTextField();
		textField_nume.setBounds(492, 50, 131, 20);
		frame.getContentPane().add(textField_nume);
		textField_nume.setColumns(10);

		textField_cantitate = new JTextField();
		textField_cantitate.setBounds(492, 75, 131, 20);
		frame.getContentPane().add(textField_cantitate);
		textField_cantitate.setColumns(10);

		textField_pret = new JTextField();
		textField_pret.setBounds(492, 100, 131, 20);
		frame.getContentPane().add(textField_pret);
		textField_pret.setColumns(10);

		JLabel lblTabelProduse = new JLabel("Tabel produse");
		lblTabelProduse.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
		lblTabelProduse.setBackground(Color.MAGENTA);
		lblTabelProduse.setBounds(120, 11, 166, 31);
		frame.getContentPane().add(lblTabelProduse);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(23, 53, 358, 305);
		frame.getContentPane().add(scrollPane);
		
		for(int i=0;i<listaProduse.size();i++) {
			model_table.addRow(new Object[]{listaProduse.get(i).getId(),listaProduse.get(i).getNume(),listaProduse.get(i).getPret(),listaProduse.get(i).getCantitate()});			
			}

		scrollPane.setColumnHeaderView(table_1);
		scrollPane.setViewportView(table_1);
		
		JButton btnReimprospatareTabelProduse = new JButton("Reimprospatare tabel produse");
		btnReimprospatareTabelProduse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				DefaultTableModel nou_model = (DefaultTableModel) table_1.getModel();
					table_1.setModel(nou_model);
			}
		});
		btnReimprospatareTabelProduse.setBounds(51, 369, 235, 31);
		frame.getContentPane().add(btnReimprospatareTabelProduse);
		
		JButton btnStergere = new JButton("Stergere");
		btnStergere.setBackground(Color.RED);
		btnStergere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					crud.stergereProdus(Integer.parseInt(textField_stergere.getText()));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnStergere.setBounds(497, 225, 99, 23);
		frame.getContentPane().add(btnStergere);
		
		JLabel lblStergereProdus = new JLabel("Stergere produs");
		lblStergereProdus.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblStergereProdus.setBounds(391, 199, 114, 14);
		frame.getContentPane().add(lblStergereProdus);
		
		textField_stergere = new JTextField();
		textField_stergere.setBounds(497, 194, 104, 20);
		frame.getContentPane().add(textField_stergere);
		textField_stergere.setColumns(10);
		
		JButton btnNewButton = new JButton("Update");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBackground(Color.RED);
		btnNewButton.setBounds(507, 335, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		textField_updateID = new JTextField();
		textField_updateID.setBounds(510, 304, 86, 20);
		frame.getContentPane().add(textField_updateID);
		textField_updateID.setColumns(10);
		
		JLabel lblUpdate = new JLabel("Update");
		lblUpdate.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblUpdate.setBounds(444, 307, 56, 14);
		frame.getContentPane().add(lblUpdate);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
}
