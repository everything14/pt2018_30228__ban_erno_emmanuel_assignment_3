package view;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import bussinessLogic.ComenziCRUD;
import model.Client;
import model.Comanda;
import model.Produs;

import java.awt.Color;

public class AdministrareComenzi {
	
	/**REFLECTION**/
	List<Field> privateFields = new ArrayList<>();
	Field[] allFields = Comanda.class.getDeclaredFields();
	int total_fields=0;

	private JFrame frame;
	private JTable table;
	private JTextField textField_nume;
	private JTextField textField_varsta;
	private JTextField textField_adresa;
	private JTextField textField_stergeID;
	ComenziCRUD crud=new ComenziCRUD();
	LinkedList<Comanda> listaProduse= new LinkedList<Comanda>();

	public AdministrareComenzi() {
		initialize();
		frame.setVisible(true);
	}

	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(224, 255, 255));
		/**REFLECTION**/
		for (Field field : allFields) {
		    if (Modifier.isPrivate(field.getModifiers())) {
		        privateFields.add(field);
		        total_fields++;
		    }
		}
		/**REFLECTION**/
		String[] columnNames=new String[total_fields];
		for(int x=0;x<privateFields.size();x++) {
			columnNames[x]=privateFields.get(x).getName();
		}
		Object[][] data = { { "9999999", "Test", "Test", "999999" } };
		JTable table = new JTable(new DefaultTableModel(data, columnNames));
		table.setForeground(new Color(0, 0, 0));
		table.setBackground(new Color(211, 211, 211));
		DefaultTableModel model_table = (DefaultTableModel) table.getModel();
		
		
		frame.setBounds(100, 100, 665, 499);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(21, 51, 266, 291);
		frame.getContentPane().add(scrollPane);
		scrollPane.setColumnHeaderView(table);
		scrollPane.setViewportView(table);
		
		JLabel lblNewLabel = new JLabel("Comenzi");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel.setBounds(130, 11, 77, 29);
		frame.getContentPane().add(lblNewLabel);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		

		/*JLabel lblNume = new JLabel("Nume");
		lblNume.setBounds(411, 49, 46, 14);
		frame.getContentPane().add(lblNume);*/
		
		JLabel lblVarsta = new JLabel("Id Client");
		lblVarsta.setBounds(411, 74, 46, 14);
		frame.getContentPane().add(lblVarsta);
		
		JLabel lblAdresa = new JLabel("Id Produs");
		lblAdresa.setBounds(411, 99, 46, 14);
		frame.getContentPane().add(lblAdresa);
		
		/*textField_nume = new JTextField();
		textField_nume.setBounds(467, 46, 86, 20);
		frame.getContentPane().add(textField_nume);
		textField_nume.setColumns(10);*/
		
		textField_varsta = new JTextField();
		textField_varsta.setBounds(467, 71, 86, 20);
		frame.getContentPane().add(textField_varsta);
		textField_varsta.setColumns(10);
		
		textField_adresa = new JTextField();
		textField_adresa.setBounds(467, 96, 86, 20);
		frame.getContentPane().add(textField_adresa);
		textField_adresa.setColumns(10);
		
		textField_stergeID = new JTextField();
		textField_stergeID.setBounds(467, 174, 86, 20);
		frame.getContentPane().add(textField_stergeID);
		textField_stergeID.setColumns(10);
		
		JButton btnAdaugaClient = new JButton("Adauga comanda");
		btnAdaugaClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Comanda c= new Comanda();
				//c.setAddresa(textField_adresa.getText());
				//c.setNume(textField_nume.getText());
				//c.setVarsta(Integer.parseInt(textField_varsta.getText()));
				c.setIdClient(Integer.parseInt(textField_varsta.getText()));
				c.setIdProdus(Integer.parseInt(textField_adresa.getText()));
				
				try {
					crud.inserareComanda(c);
					listaProduse.add(c);
					refresh();
					int i=listaProduse.size()-1;
					DefaultTableModel aux = (DefaultTableModel) table.getModel();
					aux.addRow(new Object[] { listaProduse.get(i).getId(), listaProduse.get(i).getIdClient(),
					listaProduse.get(i).getIdProdus() });
					table.setModel(aux);					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnAdaugaClient.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnAdaugaClient.setBounds(585, 70, 147, 23);
		frame.getContentPane().add(btnAdaugaClient);
		
		JButton btnNewButton = new JButton("Sterge comanda");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					crud.stergereComanda(Integer.parseInt(textField_stergeID.getText()));;
					//int i=table.getSelectedRow();
					//model_table.removeRow(i);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(585, 173, 117, 23);
		frame.getContentPane().add(btnNewButton);
		

	}
	public void refresh() throws Exception {
		this.listaProduse=crud.selectareToateComenzile();
	}
}
