package database;

import java.sql.*;

public class DataBaseTablesAndConnection {

	public static void createTable() throws Exception {

		try {
			Connection con = getConnection();

			PreparedStatement client = con.prepareStatement(
					"CREATE TABLE IF NOT EXISTS client(id int NOT NULL AUTO_INCREMENT, nume varchar(255), varsta int, adresa varchar(255), PRIMARY KEY(id))");
			PreparedStatement produs = con.prepareStatement(
					"CREATE TABLE IF NOT EXISTS produs(id int NOT NULL AUTO_INCREMENT, nume varchar(255), pret double, cantitate int, PRIMARY KEY(id))");
			PreparedStatement order = con.prepareStatement(
					"CREATE TABLE IF NOT EXISTS comanda(id int NOT NULL AUTO_INCREMENT, idClient int NOT NULL , idProdus int NOT NULL , PRIMARY KEY(id))");

			client.executeUpdate();
			produs.executeUpdate();
			order.executeUpdate();

		} catch (Exception e) {
			System.out.println("Tabelele nu au fost create");
		} finally {
			System.out.println("Tabelele au fost create cu success!!!");
		}
	}

	public static Connection getConnection() throws Exception {

		try {
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost:3306/baza_de_date?useSSL=true";
			String username = "root";
			String password = "secret19";
			Class.forName(driver);
			Connection conn = DriverManager.getConnection(url, username, password);
			System.out.println("Conexiunea la baza de date a fost facuta cu success!!");
			return conn;
		} catch (Exception e) {
			System.out.println("Erroare la conectarea cu baza de date");
		}

		return null;
	}
}
