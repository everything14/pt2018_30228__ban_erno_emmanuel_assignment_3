package bussinessLogic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

import database.DataBaseTablesAndConnection;
import model.Produs;

public class ProdusCRUD {
	
	/** INSERT ELEMENTS IN TABLE **/
	public void inserareProdus(Produs c) throws Exception {
		final String nume = c.getNume();
		final double pret = c.getPret();
		final int cantitate = c.getCantitate();
		try {
			Connection con = DataBaseTablesAndConnection.getConnection();
			PreparedStatement posted = con.prepareStatement("INSERT INTO produs(nume,pret,cantitate) VALUES ('" + nume
					+ "', '" + pret + "', '" + cantitate + "')");
			posted.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			System.out.println("Produs adaugat cu success!!");
		}
	}

	/** DELETE ELEMENTS FROM TABLE **/
	public void stergereProdus(int id) throws Exception {
		try {
			Connection con = DataBaseTablesAndConnection.getConnection();
			PreparedStatement posted = con.prepareStatement("DELETE FROM produs WHERE id=" + id);
			posted.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			System.out.println("A fost sters un produs");
		}
	}

	// ** UPDATE ELEMENTS FROM TABLE **//
	public void updateProdus(Produs c) throws Exception {
		try {
			Connection con = DataBaseTablesAndConnection.getConnection();
			PreparedStatement posted = con.prepareStatement("UPDATE produs SET nume='" + c.getNume() + "', pret='"
					+ c.getPret() + "', cantitate='" + c.getCantitate() + "' WHERE id=" + c.getId() + "");
			posted.executeUpdate();
			System.out.println("Produsul a primit update cu success");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	// VIEW A product FROM TABLE
	public Produs selectareProdusDupaID(int id) throws Exception {

		Produs c = new Produs();
		try {
			Connection con = DataBaseTablesAndConnection.getConnection();
			PreparedStatement posted = con.prepareStatement("SELECT * FROM produs WHERE id=" + id);
			ResultSet rez = posted.executeQuery();
			while (rez.next()) {
				c.setNume(rez.getString("nume"));
				c.setPret(rez.getDouble("pret"));
				c.setCantitate(rez.getInt("cantitate"));
				c.setId(id);
			}
			return c;

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			System.out.println("A fost ales un produs din baza de date");
		}
		return null;
	}

	// VIEW ALL 
	public LinkedList<Produs> selectareToateProdusele() throws Exception {
		LinkedList<Produs> lista = new LinkedList<Produs>();
		Connection con = DataBaseTablesAndConnection.getConnection();
		PreparedStatement posted = con.prepareStatement("SELECT * FROM produs ");
		ResultSet rez = posted.executeQuery();
		while (rez.next()) {
			Produs aux = new Produs(rez.getInt(1), rez.getInt(4), rez.getDouble(3), rez.getString(2));
			lista.add(aux);
		}
		return lista;
	}

}
