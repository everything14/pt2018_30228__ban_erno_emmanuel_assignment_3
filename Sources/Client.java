package model;

public class Client {

	private int id;
	private String nume;
	private int varsta;
	private String addresa;

	public Client(int id, String nume, int varsta, String addresa) {
		super();
		this.id = id;
		this.nume = nume;
		this.varsta = varsta;
		this.addresa = addresa;
	}

	public Client(String nume, int varsta, String addresa) {
		super();
		this.nume = nume;
		this.varsta = varsta;
		this.addresa = addresa;
	}

	public Client() {
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nume
	 */
	public String getNume() {
		return nume;
	}

	/**
	 * @param nume
	 *            the nume to set
	 */
	public void setNume(String nume) {
		this.nume = nume;
	}

	/**
	 * @return the ziNastere
	 */
	public int getVarsta() {
		return varsta;
	}

	/**
	 * @param ziNastere
	 *            the ziNastere to set
	 */
	public void setVarsta(int varsta) {
		this.varsta = varsta;
	}

	/**
	 * @return the addresa
	 */
	public String getAddresa() {
		return addresa;
	}

	/**
	 * @param addresa
	 *            the addresa to set
	 */
	public void setAddresa(String addresa) {
		this.addresa = addresa;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Client [id=" + id + ", nume=" + nume + ", varsta=" + varsta + ", addresa=" + addresa + "]";
	}
	
	
	

}
