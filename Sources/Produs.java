package model;

public class Produs {

	private int id;
	private int cantitate;
	private double pret;
	private String nume;

	public Produs(int id, int cantitate, double pret, String nume) {
		super();
		this.id = id;
		this.cantitate = cantitate;
		this.pret = pret;
		this.nume = nume;
	}

	public Produs(int cantitate, double pret, String nume) {
		this.cantitate = cantitate;
		this.pret = pret;
		this.nume = nume;
	}

	public Produs() {

	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the cantitate
	 */
	public int getCantitate() {
		return cantitate;
	}

	/**
	 * @param cantitate
	 *            the cantitate to set
	 */
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

	/**
	 * @return the pret
	 */
	public double getPret() {
		return pret;
	}

	/**
	 * @param pret
	 *            the pret to set
	 */
	public void setPret(double pret) {
		this.pret = pret;
	}

	/**
	 * @return the nume
	 */
	public String getNume() {
		return nume;
	}

	/**
	 * @param nume
	 *            the nume to set
	 */
	public void setNume(String nume) {
		this.nume = nume;
	}

}
