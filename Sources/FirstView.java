package view;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FirstView {

	private JFrame frame;

	public FirstView() {
		initialize();
		frame.setVisible(true);
		
	}

	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.LIGHT_GRAY);
		frame.setBounds(100, 100, 488, 328);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblBineAtiVenit = new JLabel("Bine ati venit !!");
		lblBineAtiVenit.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblBineAtiVenit.setBounds(172, 21, 142, 27);
		frame.getContentPane().add(lblBineAtiVenit);
		
		JButton btnAdministrareClienti = new JButton("Administrare Clienti");
		btnAdministrareClienti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new AdministrareClienti();
			}
		});
		btnAdministrareClienti.setBounds(10, 194, 196, 45);
		frame.getContentPane().add(btnAdministrareClienti);
		
		JButton btnAdministrareProduse = new JButton("Administrare Produse");
		btnAdministrareProduse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new AdministrareProduse();
			}
		});
		btnAdministrareProduse.setBounds(266, 194, 196, 45);
		frame.getContentPane().add(btnAdministrareProduse);
		
		JButton btnAdministrareComenzi = new JButton("Administrare Comenzi");
		btnAdministrareComenzi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent t) {
					new AdministrareComenzi();
			}
		});
		btnAdministrareComenzi.setBounds(151, 77, 163, 45);
		frame.getContentPane().add(btnAdministrareComenzi);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
}
